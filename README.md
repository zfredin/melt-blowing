# melt-blowing

design and prototyping of a FabLab-able polypropylene melt blowing setup for distributed manufacture of filter media

## status
on hold -- impractical for FabLabs due to energy needs, safety, scalability, etc

## challenges
- primary air
    - hundreds of SCFM, 200+ C, ??? psig
- tooling
- process control
    - various temperatures
    - air flow
    - feedstock extrusion
- safety
    - 200 C + pipes and tooling: burn hazard
    - Mach 0.5+ air: loud!
    - fibers: Class III/Div 1 explosion hazard?
- feedstock
    - recycled? pelletized virgin PP?
    - heat + extrude + pump?

## literature
- dimensional analysis of process and discussion of nozzle types (1988): https://pubs.acs.org/doi/pdf/10.1021/ie00084a021
- Exxon patent showing slot-style melt blowing nozzle: https://patents.google.com/patent/US3849241A/en?oq=3%2c849%2c241
- UTenn study on melt blowing: https://onlinelibrary.wiley.com/doi/epdf/10.1002/pen.760302202
- good discussion of primary air system requirements: http://jmppnet.com/journals/jmpp/Vol_3_No_1_June_2015/9.pdf

## melt blowing videos
- video of a polypropylene filter melt blowing line in action: https://www.youtube.com/watch?v=cGQ4piMNQ_E
- fast construction of a melt blowing line for covid-19 masks: https://www.youtube.com/watch?v=p9lchhBE7cE
- a smaller line with a built-in rotary mask die cutter: https://www.youtube.com/watch?v=KgWHNQo7j1k
- pilot-scale melt blowing machine making a mess: https://www.youtube.com/watch?v=SOFfpGnMjOc

## resources
- UT Knoxville Nonwovens lab: http://web.utk.edu/~tandec/default.html